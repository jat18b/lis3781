> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Justin Torres Ortega

### Assignment #5 Requirements:

1. Use MySql Server 
2. Populate Tables using T-SQL
3. Excecute SQL solutions (REPORTS)
4. Forward Engineer to Create ERD


#### README.md file should include the following items:

* Screenshot of ERD created using T-SQL
* Screenshot of Required Reports




#### Assignment Screenshots:

*Screenshot of ERD using T-SQL*:

![Screenshot of ERD](img/a5ERD.png)


*Screenshot of Reports*:

![Report 1](img/rep1.png)

![Report 2](img/rep2.png)


