> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 Advanced Database Management

## Justin Torres Ortega

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/a1_README.md "My A1 README.md file")
    - Install Homebrew.Bash (mysql for mac)
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git commands descriptions

2. [A2 README.md](a2/a2_README.md "My A2 README.md file")
    - Create a database with a Customer and Company table
    - Add/Grant User permissions
    - Populate all respective tables
    - Update Bitbucket Repository

3. [A3 README.md](a3/a3_README.md "My A3 README.md file")
    - Install Microsoft Remote Desktop: Run Oracle Server
    - Create Customer, Commodity, & Order Tables
    - Create and populate tables
    - Update Bitbucket Repository

4. [A4 README.md](a4/a4_README.md "My A4 README.md file")
    - Populate Tables using T-SQL
    - Excecute SQL solutions
    - Create ERD 
    - Update Bitbucket Repository 

5. [A5 README.md](a5/a5_README.md "My A5 README.md file")
    - Use MySQL Server 
    - Populate tables using T-SQL
    - Excecute SQL solutions
    - Create ERD
    - Update Bitbucket Repoository

6. [P1 README.md](p1/p1_README.md "My P1 README.md file")
    - Use MySql to Reverse Engineer ERD
    - Create 11 tables/ Organize ERD
    - Create Inserts and Populate tables in MySql

7. [P2 README.md](p2/p2_README.md "My P2 README.md file")
    - Install MongoDB
    - Import primer-dataset.json using mongoimport
    - Use NoSQL DBMS and MongoDB
    - Update Bitbucket Repository


### *Tables*: 
*Add table: use three or more hyphens (---) to create each column's header, and use pipes (|) to seperate each column. (optionally add pipes on either end of the table.)*
>
| Syntax    | Description |
| ----------| ----------- |
| Header    | Title       |
| Paragraph | Text        |
>
>
### Alignment: 
*Align text: left, right, or center by adding a colon (:) to left, right, or on both sides of hyphens with the header row*
>
| Syntax    | Description |  Example  |
| :---      |  :----:     |         ---: |
| left     | Center      | Right    |
>


