> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Justin Torres Ortega

### Assignment #4 Requirements:

1. Populate Tables using T-SQL
2. Excecute SQL solutions (REPORTS)
3. Create ERD


#### README.md file should include the following items:

* Screenshot of ERD created using T-SQL
* Screenshot of Required Reports




#### Assignment Screenshots:

*Screenshot of ERD using T-SQL*:

![Screenshot of ERD](img/a4_erd.png)


*Screenshot of Reports*:

![Report 1](img/rep1.png)

![Report 2](img/rep2.png)

![Report 3](img/rep3.png)

![Report 4](img/rep4.png)

![Report 5](img/rep5.png)

