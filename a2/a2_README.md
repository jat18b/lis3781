> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Justin Torres Ortega

### Assignment #2 Requirements:

*MySQL Server*

1. Create tables using SQL server only
2. Grant privileges

*Bitbucket*

1. Update bitbucket Repository


#### README.md file should include the following items:

* Screenshot of SQL code
* Screenshot of populated tables


#### Assignment Screenshots:

*Screenshot of MySql Server code Part1*:

![Screenshot of MySql running](img/a2_p1.png)

*Screenshot of MySql Server code Part2*:

![Screenshot of MySql running pt2](img/a2_p2.png)

*Screenshot of MySql Server code Part3*:

![Screenshot of MySql running pt3](img/a2_p3.png)

*Screenshot of populated tables*:

![Populated Tables](img/tables.png)
