> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Justin Torres Ortega

### Project #1 Requirements:

1. Use MySql to Reverse Engineer ERD
2. Create 11 tables/ Organize ERD
3. Create inserts and populate tables in MySql


#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of Tables in Terminal



#### Assignment Screenshots:

  **P1 ERD**

  ![P1 ERD](img/p1_ERD.png "P1 ERD")

  **Populated Tables**
  
| Populated Table 1 | Populated Table 2 |
| -----------------------------------| ----------------------------------- |
|  ![Populated Table 1](img/pop_table1.png) | ![Populated Table 2](img/pop_table2.png)|

| Populated Table 3 | Populated Table 4 |
| -----------------------------------| ----------------------------------- |
|  ![Populated Table 3](img/pop_table3.png) | ![Populated Table 4](img/pop_table4.png)|





  **P1 SQL/Inserts File**

  [p1.sql file](https://bitbucket.org/jat18b/lis4381/src/master/p1/docs/p1.sql)



