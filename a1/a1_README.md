> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 Advanced Database Management

## Justin Torres Ortega

### Assignment # 1 Requirements:

*Five Parts:*

1. Distributed Version Control with Git and Bitbucket
2. AMPPS Installation
3. Questions
4. Entity Relationship Diagram, and SQL Code
5.Bitbucket repo links:
    a) this assignment
    b) the completed tutorial (bitbucketstationlocations)


#### A1 Database Buisness Rules:

* Each employee may have one or more jobs
* Each employee has only one job
* Each job can be held by many employees 
* Many employees may recieve many benefits
* Many benefits may be selected by many employees (through they may not choose a plan, their dependants may be on a plan )
* Employee/Dependent tables must use suitable attributes(See Assignment Guidlines)

> In Addition:
> 
* Employee: SSN, DOB, Start/End dates salary;
* Dependent: same information as their associated employee, date added, and tye of relationship
* Job: title 
* Benefit: name
* Plan: type
* Employee history: jobs, salaries, and benefit changes as well as who made the changes and when 
* Zero filled data: SSN & ZIP codes
* All tables must include notes attributes
>
#### Assignment Screenshots:

*Screenshot of A1 ERD*:  

![A1 ERD](img/a1_erd.png)

*Screenshot of Homebrew.Bash/MySql Log in:

![Log In Terminal](img/log_in.png)

*Screenshot of EX1. SQL Solution*:

![EX1. SQL Solution](img/ex1_a1.png)


#### Git commands w/short descriptions:

1. git init: The git init command creates a new Git repository
2. git status: The git status command shows the current condition of the working directory and staging directory.
3. git add: The git add command adds a modification to the staging area from the working directory. It instructs Git to include changes to a certain file in the next commit.
4. git commit: git commit takes a snapshot of the project's current staged modifications.
5. git push: The git push command is used to transfer content from a local repository to a remote repository. Pushing is the process of sending commits from a local repository to a remote repository.
6. git pull: The git pull command fetches and downloads content from a remote repository and updates the local repository to match it.
7. git clean: git clean is a command that deletes untracked files from a repository's working directory.




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
