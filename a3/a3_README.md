> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Justin Torres Ortega

### Assignment #3 Requirements:

1. Install Microsoft Remote Desktop: Run Oracle Server
2. Create Customer, Commodity, & Order Tables
3. Create and populate tables


#### README.md file should include the following items:

* Screenshot of SQL code used to create and populate tables
* Screenshot of populated tables within the Oracle environment




#### Assignment Screenshots:

*Screenshot of Oracle Sql Developer code Part1*:

![Screenshot of Oracle Sql Developer running](img/a3_p1.png)

*Screenshot of Oracle Sql Developer code Part2*:

![Screenshot of Oracle Sql Developer running pt2](img/a3_p2.png)

*Screenshot of Oracle Sql Developer code Part3*:

![Screenshot of Oracle Sql Developer running pt3](img/a3_p3.png)

*Screenshot of Oracle Sql Developer code Part4*:

![Screenshot of Oracle Sql Developer running pt4](img/a3_p4.png)

*Screenshot of Customer table*:

![Custmomer Table](img/cus_table.png)

*Screenshot of Comodity table*:

![Comodity Table](img/com_table.png)

*Screenshot of order table*:

![Order Table](img/ord_table.png)
