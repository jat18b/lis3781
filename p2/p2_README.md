> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Justin Torres Ortega

### Project #2 Requirements:

1. Import primer-dataset.json using mongoimport
2. Use NoSQL DBMS and MongoDB
3. Update Bitbucket Repository

#### README.md file should include the following items:

* Screenshot of MongoDB Shell command.
* Screenshot of Required Reports



#### Assignment Screenshots:

  **P2 Mongo Shell**

  ![P2 Mongo](img/p2_mongo.png "P2 Mongo")

  **Screenshot of MongoDB Query**

  ![Query](img/query.png "Query")




