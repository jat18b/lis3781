// Project 2 Solutions 

// 1. Display all documents in a collection :
db.restaurants.find();

        // 1.b Returns one document that satisfies the criteria
            db.restaurants.findOne();

// 2. Display the number of documents in collection. 
db.restaurants.find().count()

    // 2.b Display last record both 
    db.restaurants.find()[25359]
    db.restaurants.find().sort({_id:-1}).limit(1).pretty;

// 3. Retrieve 1st 5 documents. 
db.restaurants.find().limit(5);

// 4. Retrieve restaurants in the Brooklyn borough. 
db.restaurants.find({ "borough": "Brooklyn" })

// 5. Retrieve restaurants whose cuisine is American. 
db.restaurants.find({"cuisine": "American "})
    // or using regex
    db.restaurants.find({"cuisine": /^American\s$/i})       // includes whitespace characters and case insensitive search

// 6. Retrieve restaurants whose borough is Manhattan and cuisine is hamburgers. 
db.restaurants.find({ "borough": "Manhattan", "cuisine": "Hamburgers" })

// 7. Display the number of restaurants whose borough is Manhattan and cuisine is hamburgers. 
db.restaurants.find({ "borough": "Manhattan", "cuisine": "Hamburgers" }).count()

/* 8. Query zipcode field in embedded address document. 
      Retrieve restaurants in the 10075 zip code area. 
*/
db.restaurants.find( { "adress.zipcode": "10075" } )

// 9. Retrieve restaurants whose cuisine is chicken and zip code is 10024. 
db.restaurants.find({"cuisine": "Chicken"}, {"address.zipcode": "10024"})       // implicit AND operation

    // equivalent to "AND" ...(NOTE: $AND/$OR used with "array" square brackets)
    db.restaurants.find({ $and: [{"cuisine": "Chicken"}, {"address.zipcode": "10024"}]})
// 10. Retrieve restaurants whose cuisine is chicken or whose zip code is 10024. 
db.restaurants.find({ $or: [{"cuisine": "Chicken"}, {"address.zipcode": "10024"}]})


/* 11. Retrieve restaurants whose borough is Queens, cuisine is Jewish/kosher, sort by descending order 
       of zipcode. 
*/
db.restaurants.find({"borough": "Queens", "cuisine": "Jewish/Kosher"}).sort({"adress.zipcode":-1})

// 12. Retrieve restaurants with a grade A. 
db.restaurants.find({"grades.grade": "A"})

// 13. Retrieve restaurants with a grade A, displaying only collection id, restaurant name, and grade. 
db.restaurants.find({"grades.grade": "A"}, {"name": 1, "grades.grade": 1})

// 14. Retrieve restaurants with a grade A, displaying only restaurant name, and grade (no collection id): 
db.restaurants.find({"grades.grade": "A"}, {"name": 1, "grades.grade": 1, _id: 0})

// 15. Retrieve restaurants with a grade A, sort by cuisine ascending, and zip code descending. 
db.restaurants.find({"grades.grade": "A"}).sort({"cuisine": 1, "address.zipcode": -1})

// 16. Retrieve restaurants with a score higher than 80. 
db.restaurants.find({"grades.score": {$gt: 80}})

// 17. Insert a record with the following data: 
street = 7th Avenue 
zip code = 10024 
building = 1000 
coord = -58.9557413, 31.7720266 
borough = Brooklyn 
cuisine = BBQ 
date = 2015-11-05T00:00:00Z 
grade" : C 
score = 15 
name = Big Tex 
restaurant_id = 61704627

// Step 1: Review data to display format:
db.restaurants.find().limit(1).pretty()

// " " : " ",
// Step 2: Then insert...
db.restaurants.insert(
{
"address" : {
"street" : "7th Avenue",
"zipcode" : "10024",
"building" : "1000",
"coord" : [ -58.9557413, 31.7720266 ],
},
"borough" : "Brooklyn",
"cuisine" : "BBQ",
"grades" : [
{
"date" : ISODate("2015-11-05T00:00Z"),
"grade" : "C",
"score" : 15,
}
],
"name" : "Big Tex",
"restaurant_id" : "61704627",
}
)

// Step 3: Verify insert (should be last record/document):
db.restaurants.find().sort({_id:-1}).limit(1).pretty();


/* 18. Update the following record: 
Change the first White Castle restaurant document's cuisine to "Steak and Sea Food," and update the 
lastModified field with the current date. 
 */

// Review data: pretty() format(easy read)
db.restaurants.find({"name" : "White Castle"}).limit(1).pretty()

// use _id (primary key) attribute 
db.restaurants.update(
{"_id" : ObjectId("625dca30b2df59cccfa61429")},
{
// Note: original does *NOT* have a lastModified field -- *NOW* it will!
$set: {"cuisine": "Steak and Sea Food"}, $currentDate: {"lastModified": true}
}
)



/* 19. Delete the following records: 
Delete all White Castle restaurants. 
*/

// Step 1: Display number of documents before delete
db.restaurants.find({"name" : "White Castle"}, {"name" : 1, "cuisine": 1, _id: 0}).count()

// Step 2: Delete documents ***ANSWER***
db.restaurants.remove({"name" : "White Castle"})

// Step 3: Display number of documents after delete
db.restaurants.find({"name" : "White Castle"}, {"name" : 1, "cuisine": 1, _id: 0}).count()